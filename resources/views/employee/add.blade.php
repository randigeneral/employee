@extends('layout.root')

@section('content')
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Tambah Karyawan</h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <!-- Content area -->
        <div class="content pt-0">
            <!-- Sidebars overview -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="form-add" class="form-validate-jquery">
                                <fieldset class="mb-3">
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-2">Nama<span class="text-danger">*</span></label>
                                        <div class="col-lg-10">
                                            <input class="form-control" name="nama" id="nama" placeholder="" type="text" required="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-2">NIK<span class="text-danger">*</span></label>
                                        <div class="col-lg-10">
                                            <input class="form-control" name="nik" id="nik" placeholder="" type="text" required="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-2">Tanggal Lahir<span class="text-danger">*</span></label>
                                        <div class="col-lg-10">
                                            <input class="form-control" name="tanggal_lahir" id="tanggal_lahir" placeholder="" type="date" required="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-2">Alamat<span class="text-danger">*</span></label>
                                        <div class="col-lg-10">
                                            <textarea class="form-control" name="alamat" id="alamat" required=""></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-2">Pekerjaan<span class="text-danger">*</span></label>
                                        <div class="col-lg-10">
                                            <input class="form-control" name="pekerjaan" id="pekerjaan" placeholder="" type="text" required="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-2">Departemen<span class="text-danger">*</span></label>
                                        <div class="col-lg-10">
                                            <input class="form-control" name="department" id="department" placeholder="" type="text" required="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-2">Jenis Kelamin<span class="text-danger">*</span></label>
                                        <div class="col-lg-10">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" id="laki" name="jenis_kelamin" id="exampleRadios1" value="L" checked>
                                                <label class="form-check-label" for="exampleRadios1">Laki-laki</label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" id="perempuan"  name="jenis_kelamin" id="exampleRadios2" value="P">
                                                <label class="form-check-label" for="exampleRadios2">Perempuan</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-2">Kota<span class="text-danger">*</span></label>
                                        <div class="col-lg-10">
                                            <input class="form-control" name="kota" id="kota" placeholder="" type="text" required="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-2">Tanggal Bergabung<span class="text-danger">*</span></label>
                                        <div class="col-lg-10">
                                            <input class="form-control" name="join_date"  id="join_date" placeholder="" type="date" required="">
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="text-right">
                                    <a href="{{ url('/employee') }}" class="btn btn-sm btn-secondary">Kembali</a>
                                    <button type="submit" class="btn btn-sm btn-primary legitRipple save-data" id="submit">Simpan <i class="icon-paperplane ml-2"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{ url('/') }}/assets/global/js/demo_pages/form_validation.js"></script>
<script src="{{ url('/') }}/assets/global/js/pages/employeeAdd.js"></script>
@endpush


    