@extends('layout.root')

@section('content')
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Detail Karyawan</h4>
                    <input type="hidden" name="employee_id" id="employee_id" value="{{ $id }}">
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <!-- Content area -->
        <div class="content pt-0">
            <!-- Sidebars overview -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>Nama</td>
                                        <td> &nbsp; : &nbsp; </td>
                                        <td id="nama"></td>
                                    </tr>
                                    <tr>
                                        <td>NIK</td>
                                        <td> &nbsp; : &nbsp; </td>
                                        <td id="nik"></td>
                                    </tr>
                                    <tr>
                                        <td>Pekerjaan</td>
                                        <td> &nbsp; : &nbsp; </td>
                                        <td id="pekerjaan"></td>
                                    </tr>
                                    <tr>
                                        <td>Departemen</td>
                                        <td> &nbsp; : &nbsp; </td>
                                        <td id="department"></td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td> &nbsp; : &nbsp; </td>
                                        <td id="alamat"></td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Kelamin</td>
                                        <td> &nbsp; : &nbsp; </td>
                                        <td id="gender"></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Lahir</td>
                                        <td> &nbsp; : &nbsp; </td>
                                        <td id="tanggal_lahir"></td>
                                    </tr>
                                    <tr>
                                        <td>Kota</td>
                                        <td> &nbsp; : &nbsp; </td>
                                        <td id="kota"></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Masuk</td>
                                        <td> &nbsp; : &nbsp; </td>
                                        <td id="tanggal_masuk"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row pt-5"> 
                        <div class="col-md-12">
                            <a href="{{ url('/employee') }}" class="btn btn-sm btn-secondary">Kembali</a>
                            <a href="{{ url('/employee/edit/'.$id) }}" class="btn btn-sm btn-primary">Edit</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{ url('/') }}/assets/global/js/demo_pages/form_validation.js"></script>
<script src="{{ url('/') }}/assets/global/js/pages/employeeDetail.js"></script>
@endpush


    