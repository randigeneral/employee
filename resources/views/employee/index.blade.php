@extends('layout.root')

@section('content')
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Data Karyawan</h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <!-- Content area -->
        <div class="content pt-0">
            <div class="row mb-3">
                <div class="col-md-12 text-right">
                    <a href="{{ url('/employee/add') }}" class="btn btn-sm btn-primary">Tambah Karyawan</a>
                </div>
            </div>

            <!-- Sidebars overview -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        
                        <div class="col-md-12">
                            <table class="table table bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>NIK</th>
                                        <th>Tanggal Lahir</th>
                                        <th>Alamat</th>
                                        <th>Pekerjaan</th>
                                        <th>Departemen</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Kota</th>
                                        <th>Tanggal Masuk</th>
                                        <th class="text-center">#</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                      $no=1;  
                                    @endphp
                                    @foreach ($employee as $e)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $e->nama }}</td>
                                            <td>{{ $e->nik }}</td>
                                            <td>{{ Helper::tgl_indo($e->tanggal_lahir) }}</td>
                                            <td>{{ $e->alamat }}</td>
                                            <td>{{ $e->job_title }}</td>
                                            <td>{{ $e->department }}</td>
                                            <td>{{ ($e->gender == 'L')?"Laki-laki":"Perempuan" }}</td>
                                            <td>{{ $e->kota }}</td>
                                            <td>{{ Helper::tgl_indo($e->join_date) }}</td>
                                            <td>
                                                <a href="{{ url('/employee/detail/'.$e->id) }}" class="btn btn-sm btn-success">Detail</a>    
                                                <a href="{{ url('/employee/edit/'.$e->id) }}" class="btn btn-sm btn-primary">Edit</a>    
                                                <a href="#" data-id={{ $e->id }} data-nama={{ $e->nama }} class="btn btn-sm btn-danger btn-delete">Delete</a>    
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{ url('/') }}/assets/global/js/demo_pages/form_validation.js"></script>
<script src="{{ url('/') }}/assets/global/js/pages/employee.js"></script>
@endpush


    