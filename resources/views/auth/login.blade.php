<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login</title>

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/images/pavicon-biosm.png')}}">
    <title>@yield('title') {{config('app.name')}}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <link href="{{url('/')}}/assets/global/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="{{url('/')}}/assets/layout/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="{{url('/')}}/assets/layout/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="{{url('/')}}/assets/layout/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="{{url('/')}}/assets/layout/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="{{url('/')}}/assets/layout/css/colors.min.css" rel="stylesheet" type="text/css">
    <script src="{{url('/')}}/assets/global/js/main/jquery.min.js"></script>
    <script src="{{url('/')}}/assets/global/js/main/bootstrap.bundle.min.js"></script>
    <script src="{{url('/')}}/assets/global/js/plugins/loaders/blockui.min.js"></script>
    <script src="{{url('/')}}/assets/global/js/plugins/notifications/pnotify.min.js"></script>
    <script src="{{url('/')}}/assets/global/js/plugins/forms/validation/validate.min.js"></script>
    <script src="{{url('/')}}/assets/global/js/plugins/forms/styling/uniform.min.js"></script>
    <script src="{{url('/')}}/assets/layout/js/app.js"></script>
    <script src="{{url('/')}}/assets/global/js/demo_pages/login_validation.js"></script>
    
</head>
<body>
    <div class="page-content">
        <div class="content-wrapper">
            <div class="content d-flex justify-content-center align-items-center">
                <form action="{{ url('api/auth/login') }}" id="form-login" method="POST">
                    <div class="card mb-0" style="width: 300px;">
                        <div class="card-body">
                            <div class="text-center mb-3">
                                <i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
                                <h5 class="mb-0">Login to your account</h5>
                                <span class="d-block text-muted">Enter your credentials below</span>
                            </div>
                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="text" name="email" class="form-control" placeholder="Username" required>
                                <div class="form-control-feedback">
                                    <i class="icon-user text-muted"></i>
                                </div>
                            </div>
                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="password" name="password" class="form-control" placeholder="Password" required>
                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" id="btn-login submit" class="btn btn-primary btn-block">Login</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script>
        var baseurl = "{{url('/')}}";
    </script>
    <script src="{{url('/')}}/assets/global/js/pages/login.js"></script>
</body>
</html>

