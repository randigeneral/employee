@extends('layout.dashboard')

@section('content')
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Dashboard</h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <!-- Content area -->
        <div class="content pt-0">
            <!-- Basic pie charts -->
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header header-elements-inline">
                            <h5 class="card-title">Pie Chart Karyawan</h5>
                            <div class="header-elements">
                                <div class="list-icons">
                                    <a class="list-icons-item" data-action="collapse"></a>
                                    <a class="list-icons-item" data-action="reload"></a>
                                    <a class="list-icons-item" data-action="remove"></a>
                                </div>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="chart-container text-center">
                                <div class="d-inline-block" id="google-pie"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <!-- Column chart -->
                    <div class="card">
                        <div class="card-header header-elements-inline">
                            <h5 class="card-title">Rentang Umur Karyawan</h5>
                            <div class="header-elements">
                                <div class="list-icons">
                                    <a class="list-icons-item" data-action="collapse"></a>
                                    <a class="list-icons-item" data-action="reload"></a>
                                    <a class="list-icons-item" data-action="remove"></a>
                                </div>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="chart-container">
                                <div class="chart" id="google-column"></div>
                            </div>
                        </div>
                    </div>
                    <!-- /column chart -->
                </div>
            </div>
            <!-- /basic pie charts -->
        </div>
    </div>
@endsection
    
@push('scripts')
<script src="{{ url('/') }}/assets/global/js/pages/dashboard.js"></script>
@endpush
