<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/')}}">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{url('/')}}/assets/global/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="{{url('/')}}/assets/layout/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="{{url('/')}}/assets/layout/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="{{url('/')}}/assets/layout/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="{{url('/')}}/assets/layout/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="{{url('/')}}/assets/layout/css/colors.min.css" rel="stylesheet" type="text/css">
    <link href="{{url('/')}}/assets/layout/css/style.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    @yield('styles')
</head>
<body>

    <!-- Main navbar -->
    <div class="navbar navbar-expand-md navbar-light navbar-static">

        <!-- Header with logos -->
        <div class="navbar-header navbar-dark d-none d-md-flex align-items-md-center">
            <div class="navbar-brand navbar-brand-md">
                <a href="index.html" class="d-inline-block">
                    <img src="{{url('/')}}/assets/global/images/logo_light.png" alt="">
                </a>
            </div>

            <div class="navbar-brand navbar-brand-xs">
                <a href="index.html" class="d-inline-block">
                    <img src="{{url('/')}}/assets/global/images/logo_icon_light.png" alt="">
                </a>
            </div>
        </div>
        <!-- /header with logos -->

        <!-- Navbar content -->
        <div class="collapse navbar-collapse" id="navbar-mobile">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                        <i class="icon-paragraph-justify3"></i>
                    </a>
                </li>
            </ul>

            <span class="badge bg-pink-400 badge-pill ml-md-3 mr-md-auto">16 orders</span>

            <ul class="navbar-nav">
                <li class="nav-item dropdown dropdown-user">
                    <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                        <img src="{{url('/')}}/assets/global/images/placeholders/placeholder.jpg" class="rounded-circle mr-2" height="34" alt="">
                        {{-- <span>{{ session('user_name') }}</span> --}}
                        <span>{!! Helper::getActiveUser()->user_firstname.' '.Helper::getActiveUser()->user_lastname !!}</span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="{{ url('logout')}}" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
                    </div>
                </li>
            </ul>
        </div>
        <!-- /navbar content -->

    </div>
    <!-- /main navbar -->


    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

            <!-- Sidebar content -->
            <div class="sidebar-content">

                <!-- Main navigation -->
                <div class="card card-sidebar-mobile">
                    <ul class="nav nav-sidebar" data-nav-type="accordion">

                        <!-- Main -->
                        <li class="nav-item-header">
                            <div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i>
                        </li>
                        <li class="nav-item">
                            <a href='{{ url("/dashboard") }}' class="nav-link"> <i class="icon-home4"></i><span>Dashboard</span></a>
                        </li>
                        <li class="nav-item {{ (!empty($menu) && $menu == 'Request') ? 'nav-item-expanded nav-item-open' : ''}}">
                            <a href="{{ url("/employee") }}" class="nav-link"><i class="icon-users"></i> <span>Karyawan</span></a>
                        </li>

                    </ul>
                </div>
                <!-- /main navigation -->

            </div>
            <!-- /sidebar content -->

        </div>
        <!-- /main sidebar -->


        @yield('content')

    </div>
    <!-- /page content -->

    <!-- Core JS files -->
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src="{{url('/')}}/assets/global/js/main/jquery.min.js"></script>
    <script src="{{url('/')}}/assets/global/js/main/bootstrap.bundle.min.js"></script>
    <script src="{{url('/')}}/assets/global/js/plugins/loaders/blockui.min.js"></script>
    <script src="{{url('/')}}/assets/global/js/plugins/ui/ripple.min.js"></script>
    <script src="{{url('/')}}/assets/global/js/plugins/forms/validation/validate.min.js"></script>
    <script src="{{url('/')}}/assets/global/js/pages/form.js"></script>
    <!-- /core JS files -->
    <!-- /theme JS files -->
    <script>
        var BASE_URL = "{{url('/')}}";
        var baseurl = "{{url('/')}}";
    </script>
    @stack('scripts')

</body>
</html> 