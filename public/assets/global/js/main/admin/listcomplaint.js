var tableProject = null;

jQuery(document).ready(function ($) {
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        order: [],
        columnDefs: [
            {
                // orderable: true,
                width: 100,
                // order: [[0, "desc"]],
                "targets"  : 'no-sort',
                "orderable": false,
            },
        ],
        dom:
            '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: "",
            searchPlaceholder: "Type to filter...",
            lengthMenu: "<span>Show:</span> _MENU_",
            paginate: {
                first: "First",
                last: "Last",
                next: $("html").attr("dir") == "rtl" ? "&larr;" : "&rarr;",
                previous: $("html").attr("dir") == "rtl" ? "&rarr;" : "&larr;",
            },
        },
    });


    // Basic datatable
    tableProject = $("#mytable").DataTable({
        processing: true,
        serverSide: true,
        filter: true,
        orderMulti: true,
        bLengthChange: false,
        scrollX: true,

        ajax: {
            url: baseurl + "/admin-console/complaint/show",
            dataType: "json",
            type: "GET",
            data: function (d) {
                // d.loc_id = $('#loc_id').val();
            },
        },
        columns: [
            {
                data: "purchase_id",
                name: "purchase_id",
            },
            {
                data: "complaint_desc",
                name: "complaint_desc",
            },
            {
                data: "created_at",
                name: "created_at",
            },
            {
                data: "buyer_name",
                name: "buyer_name",
            },
            {
                data: "pelapak_name",
                name: "pelapak_name",
            },
            {
                data: "product_name",
                name: "product_name",
            },
            {
                data: "status_descr",
                name: "status_descr",
                render: function (value, type, row) {
                    string =
                        value == "Aktif"
                            ? '<span class="badge badge-success">' +
                              value +
                              "</span>"
                            : '<span class="badge badge-warning">' +
                              value +
                              "</span>";
                    return string;
                },
            },
            {
                data: "action",
                name: "action",
            },
        ],
    });

    // $("#mytable").on("click", "tbody tr", function () {
    //     var value = tableProject.row($(this)).data()["cat_id"];
    //     link = baseurl + "/admin-console/category/detail/" + value;
    //     window.location.href = link;
    // });

    $("#mytable").on("click", ".btn-delete", function () {
        // $("#delete-form")[0].reset();
        $("#deletepop").modal("show");
        $("input[name='brand_id']").val($(this).data("brand_id"));
        $("#brand-name").html($(this).data("brand_name"));
    });

    $(".confirm-delete-data").on("click", function () {
        deleteData();
    });

    function deleteData() {
        var fd = $("#delete-form").serializeArray();
        var apiurl = baseurl + "/admin-console/brand/delete";
        $.ajax({
            url: apiurl,
            type: "post",
            data: fd,
            beforeSend: function () {
                $("#deletepop").block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: "#fff",
                        opacity: 0.8,
                        cursor: "wait",
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: "none",
                    },
                });
            },
            success: function (json) {
                $("#deletepop").unblock();
                if (json.response.status == true) {
                    new PNotify({
                        title: "success!",
                        text: json.response.message,
                        addclass: "bg-success",
                    });
                    window.location.href =
                        baseurl + "/admin-console/brand/lists";
                } else {
                    new PNotify({
                        title: "failed!",
                        text: json.response.message,
                        addclass: "bg-danger",
                    });
                }
            },
            error: function (xhr) {
                new PNotify({
                    title: "Failed!",
                    text: xhr,
                    addclass: "bg-danger",
                });
                $("#deletepop").unblock();
            },
        });
    }
});

function change() {
    tableProject.draw();
}
