var tableProject = null;

jQuery(document).ready(function ($) {
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        columnDefs: [
            {
                orderable: false,
                width: 100,
                order: [[0, "desc"]],
            },
        ],
        dom:
            '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: "",
            searchPlaceholder: "Type to filter...",
            lengthMenu: "<span>Show:</span> _MENU_",
            paginate: {
                first: "First",
                last: "Last",
                next: $("html").attr("dir") == "rtl" ? "&larr;" : "&rarr;",
                previous: $("html").attr("dir") == "rtl" ? "&rarr;" : "&larr;",
            },
        },
    });

    // Basic datatable
    tableProject = $("#mytable").DataTable({
        processing: true,
        serverSide: true,
        filter: true,
        orderMulti: true,
        bLengthChange: false,
        scrollX: true,

        ajax: {
            url: baseurl + "/admin-console/user/show",
            dataType: "json",
            type: "GET",
            data: function (d) {
                // d.loc_id = $('#loc_id').val();
            },
        },
        columns: [
            {
                data: "user_id",
                name: "user_id",
                visible: false,
                searchable: false,
            },
            {
                data: "image",
                name: "image",
            },
            {
                data: "name",
                name: "name",
            },
            {
                data: "user_email",
                name: "user_email",
            },
            {
                data: "user_phone",
                name: "user_phone",
            },
            {
                data: "jenis_kelamin",
                name: "jenis_kelamin",
            },
            {
                data: "status",
                name: "status",
            },
            {
                data: "action",
                name: "action",
            },
        ],
    });

    // $("#mytable").on("click", "tbody tr", function () {
    //     var value = tableProject.row($(this)).data()["cat_id"];
    //     link = baseurl + "/admin-console/category/detail/" + value;
    //     window.location.href = link;
    // });

    $("#mytable").on("click", ".btn-delete", function () {
        // $("#delete-form")[0].reset();
        $("#deletepop").modal("show");
        $("input[name='brand_id']").val($(this).data("brand_id"));
        $("#brand-name").html($(this).data("brand_name"));
    });

    $(".confirm-delete-data").on("click", function () {
        deleteData();
    });

    function deleteData() {
        var fd = $("#delete-form").serializeArray();
        var apiurl = baseurl + "/admin-console/brand/delete";
        $.ajax({
            url: apiurl,
            type: "post",
            data: fd,
            beforeSend: function () {
                $("#deletepop").block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: "#fff",
                        opacity: 0.8,
                        cursor: "wait",
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: "none",
                    },
                });
            },
            success: function (json) {
                $("#deletepop").unblock();
                if (json.response.status == true) {
                    new PNotify({
                        title: "success!",
                        text: json.response.message,
                        addclass: "bg-success",
                    });
                    window.location.href =
                        baseurl + "/admin-console/brand/lists";
                } else {
                    new PNotify({
                        title: "failed!",
                        text: json.response.message,
                        addclass: "bg-danger",
                    });
                }
            },
            error: function (xhr) {
                new PNotify({
                    title: "Failed!",
                    text: xhr,
                    addclass: "bg-danger",
                });
                $("#deletepop").unblock();
            },
        });
    }
});

function change() {
    tableProject.draw();
}
