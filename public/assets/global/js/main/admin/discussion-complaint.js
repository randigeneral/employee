$(document).ready(function() {
    $('.select2').select2();
    $('#complaint_discus_text_buyer').summernote({
        height: 300,
        placeholder: 'Masukan tanggapanmu disini',
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['insert', ['link']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ]
    });

    $('#complaint_discus_text_seller').summernote({
        height: 300,
        placeholder: 'Masukan tanggapanmu disini',
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['insert', ['link']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ]
    });

    $('#btn-buyer-submit').click(function(){
        if (!$('#complaint_discus_text_buyer').val()) {
            new PNotify({
                title: "Failed!",
                text: 'Isikan tanggapan terlebih dahulu',
                addclass: "bg-danger",
            });
            $('#complaint_discus_text_buyer').focus();
        }else{
            $.ajax({
                type: $('#buyer-discuss-form').attr('method'),
                url: $('#buyer-discuss-form').attr('action'),
                data: {
                    'complaint_discus_text' : $('#complaint_discus_text_buyer').val(),
                    'is_from_admin' : '1',
                    'complaint_id' : $('#complaint_id').val(),
                    'status_user' : 'buyer',
                    'complaint_status' : $('#complaint_status').val()
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function() {
                    $('.tab-content').block({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'none'
                        }
                    });
                },
                success: function(data) {
                    console.log(data);
                    if ((data.response.status) && (data.response.code == 200)) {
                        new PNotify({
                            title: 'success',
                            text: data.response.message,
                            addclass: 'bg-success'
                        });
                        setTimeout(function() { window.location.reload() }, 1000);
                    } else {
                        new PNotify({
                            title: 'Failed!',
                            text: data.response.Message,
                            addclass: 'bg-danger'
                        });
                    }
                    $('.tab-content').unblock();
                },
                error: function(xhr) {
                    new PNotify({
                        title: 'Failed!',
                        text: xhr.responseJSON.response.message,
                        addclass: 'bg-danger'
                    });
                    $('.tab-content').unblock();
                },
                dataType: 'json'
            });
        }
    });

    $('#btn-seller-submit').click(function(){
        console.log('seller submit');
        if (!$('#complaint_discus_text_seller').val()) {
            new PNotify({
                title: "Failed!",
                text: 'Isikan tanggapan terlebih dahulu',
                addclass: "bg-danger",
            });
            $('#complaint_discus_text_seller').focus();
        }else{
            $.ajax({
                type: $('#seller-discuss-form').attr('method'),
                url: $('#seller-discuss-form').attr('action'),
                data: {
                    'complaint_discus_text' : $('#complaint_discus_text_seller').val(),
                    'is_from_admin' : '1',
                    'complaint_id' : $('#complaint_id').val(),
                    'status_user' : 'seller',
                    'complaint_solution' : $('#complaint_solution').val()
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function() {
                    $('.tab-content').block({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'none'
                        }
                    });
                },
                success: function(data) {
                    console.log(data);
                    if ((data.response.status) && (data.response.code == 200)) {
                        new PNotify({
                            title: 'success',
                            text: data.response.message,
                            addclass: 'bg-success'
                        });
                        setTimeout(function() { window.location.reload() }, 1000);
                    } else {
                        new PNotify({
                            title: 'Failed!',
                            text: data.response.Message,
                            addclass: 'bg-danger'
                        });
                    }
                    $('#complaint_discus_text_seller').code('');
                    $('.tab-content').unblock();
                },
                error: function(xhr) {
                    new PNotify({
                        title: 'Failed!',
                        text: xhr.responseJSON.response.message,
                        addclass: 'bg-danger'
                    });
                    $('.tab-content').unblock();
                },
                dataType: 'json'
            });
        }
    });

});
