var tableProject = null;

jQuery(document).ready(function ($) {
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        columnDefs: [
            {
                orderable: false,
                width: 100,
                order: [[0, "desc"]],
            },
        ],
        dom:
            '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: "",
            searchPlaceholder: "Type to filter...",
            lengthMenu: "<span>Show:</span> _MENU_",
            paginate: {
                first: "First",
                last: "Last",
                next: $("html").attr("dir") == "rtl" ? "&larr;" : "&rarr;",
                previous: $("html").attr("dir") == "rtl" ? "&rarr;" : "&larr;",
            },
        },
    });

    // Basic datatable
    tableProject = $("#mytable").DataTable({
        processing: true,
        serverSide: true,
        filter: true,
        orderMulti: true,
        bLengthChange: false,
        scrollX: true,
        order:[[0,'desc']],

        ajax: {
            url: baseurl + "/admin-console/voucher/show",
            dataType: "json",
            type: "GET",
            data: function (d) {
                // d.loc_id = $('#loc_id').val();
            },
        },
        columns: [
            {
                data: "created_at",
                name: "created_at",
                visible: false,
                searchable: false,
            },
            {
                data: "image",
                name: "image",
            },
            {
                data: "name",
                name: "name",
            },
            {
                data: "discount",
                name: "discount",
            },
            {
                data: "quota",
                name: "quota",
            },
            {
                data: "number_used",
                name: "number_used",
            },
            {
                data: "used",
                name: "used",
            },
            {
                data: "status",
                name: "status",
            },
            {
                data: "action",
                name: "action",
            },
        ],
    });

    $("#mytable").on("click", ".btn-delete", function () {
        // $("#delete-form")[0].reset();
        $("#deletepop").modal("show");
        $("input[name='voucher_id']").val($(this).data("voucher_id"));
        $("#voucher-name").html($(this).data("voucher_name"));
    });

    $(".confirm-delete-data").on("click", function () {
        deleteData();
    });

    function deleteData() {
        var fd = $("#delete-form").serializeArray();
        var apiurl = baseurl + "/admin-console/voucher/delete";
        $.ajax({
            url: apiurl,
            type: "post",
            data: fd,
            beforeSend: function () {
                $("#deletepop").block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: "#fff",
                        opacity: 0.8,
                        cursor: "wait",
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: "none",
                    },
                });
            },
            success: function (json) {
                $("#deletepop").unblock();
                if (json.response.status == true) {
                    new PNotify({
                        title: "success!",
                        text: json.response.message,
                        addclass: "bg-success",
                    });
                    window.location.href =
                        baseurl + "/admin-console/voucher/lists";
                } else {
                    new PNotify({
                        title: "failed!",
                        text: json.response.message,
                        addclass: "bg-danger",
                    });
                }
            },
            error: function (xhr) {
                new PNotify({
                    title: "Failed!",
                    text: xhr,
                    addclass: "bg-danger",
                });
                $("#deletepop").unblock();
            },
        });
    }
});

function change() {
    tableProject.draw();
}
