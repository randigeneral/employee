var tableProject = null;

jQuery(document).ready(function ($) {

    $("#main-form").on("submit", function (e) {
        // check if the input is valid
        e.preventDefault();
        if (!$(this).valid()) return false;
    
        var fd = $("#main-form").serializeArray();
        var apiurl = baseurl + "/admin-console/auth/login";
        if ($("#main-form").valid()) {
            $.ajax({
                url: apiurl,
                type: "post",
                data: fd,
                beforeSend: function () {
                    $("#btn-login").block({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: "#fff",
                            opacity: 0.8,
                            cursor: "wait",
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: "none",
                        },
                    });
                },
                success: function (json) {
                    $("#btn-login").unblock();
                    if (json.response.status == true) {
                        new PNotify({
                            title: "success!",
                            text: json.response.message,
                            addclass: "bg-success",
                        });
                        window.location.href =
                            baseurl + "/admin-console/category/lists";
                    } else {
                        new PNotify({
                            title: "failed!",
                            text: json.response.message,
                            addclass: "bg-danger",
                        });
                    }
                },
                error: function (xhr) {
                    $("#btn-login").unblock();
                    new PNotify({
                        title: "Failed!",
                        text: xhr.responseJSON.response.message,
                        addclass: "bg-danger",
                    });
                },
            });
        } else {
            new PNotify({
                title: "Validasi Error!",
                text: "Isian data masih ada yang kurang, silahkan perbaiki.",
                addclass: "bg-danger",
            });
        }
    });
   
});



