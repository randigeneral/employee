var tableProject = null;

jQuery(document).ready(function ($) {
    // Tagging support
    $("#category_id").select2();
    $("#subcategory_id").select2();

    $("#category_id").change(function() {
        let category_id = $(this).val();
        $("#subcategory_id").select2({
            placeholder: "-- Pilih Sub Kategori --",
            width: "100%",
            // dropdownParent: $("#responsive-modal"),
            ajax: {
                url: baseurl + "/admin-console/subsubcategory/listsubcategory/" + category_id,
                processResults: function(data) {
                    var dataList = $.map(data.data, function(obj) {
                        obj.id = obj.cat_id;
                        obj.text = obj.cat_name;
                        return obj;
                    });
                    return {
                        results: dataList,
                    };
                },
            },
        });
    });

    $("#subcategory_id").change(function() {
        let subcategory_id = $(this).val();
        $("#select-multiple-tags").select2({
            tags: true,
            placeholder: "-- Pilih Sub Kategori --",
            width: "100%",
            // dropdownParent: $("#responsive-modal"),
            ajax: {
                url: baseurl + "/admin-console/subsubcategory/listsubsubcategory/" + subcategory_id,
                processResults: function(data) {
                    var dataList = $.map(data.data, function(obj) {
                        obj.id = obj.cat_id;
                        obj.text = obj.cat_name;
                        return obj;
                    });
                    return {
                        results: dataList,
                    };
                },
            },
        });
    });

    $('#select-multiple-tags').select2({
        tags: true
    });
});

var image = null;

$("input[name=brand_image]").on("change", function () {
    getBase64(this, "cat_image");
});

function getBase64(file, target) {
    var reader = new FileReader();
    reader.readAsDataURL(file.files[0]);
    reader.onload = function () {
        if (target == "cat_image") {
            image = reader.result.replace(/^[^,]*,/, "");
        }
    };
    reader.onerror = function (error) {};
}

$("#main-form").on("submit", function (e) {
    // check if the input is valid
    e.preventDefault();
    if (!$(this).valid()) return false;

    var fd = $("#main-form").serializeArray();
    fd.push({ name: "brand_image", value: image });

    var apiurl = baseurl + "/admin-console/brand/add";
    if ($("#main-form").valid()) {
        $.ajax({
            url: apiurl,
            type: "post",
            data: fd,
            beforeSend: function () {
                $(".content").block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: "#fff",
                        opacity: 0.8,
                        cursor: "wait",
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: "none",
                    },
                });
            },
            success: function (json) {
                $(".content").unblock();
                if (json.response.status == true) {
                    new PNotify({
                        title: "success!",
                        text: json.response.message,
                        addclass: "bg-success",
                    });
                    window.location.href =
                        baseurl + "/admin-console/brand/lists";
                } else {
                    new PNotify({
                        title: "failed!",
                        text: json.response.message,
                        addclass: "bg-danger",
                    });
                }
            },
            error: function (xhr) {
                new PNotify({
                    title: "Failed!",
                    text: xhr,
                    addclass: "bg-danger",
                });
                $(".content").unblock();
            },
        });
    } else {
        new PNotify({
            title: "Validasi Error!",
            text: "Isian data masih ada yang kurang, silahkan perbaiki.",
            addclass: "bg-danger",
        });
    }
});
