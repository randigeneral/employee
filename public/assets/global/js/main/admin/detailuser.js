var tableProject = null;

jQuery(document).ready(function ($) {
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        order: [],
        columnDefs: [
            {
                width: 100,
                "targets"  : 'no-sort',
                "orderable": false,
            },
        ],
        dom:
            '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: "",
            searchPlaceholder: "Type to filter...",
            lengthMenu: "<span>Show:</span> _MENU_",
            paginate: {
                first: "First",
                last: "Last",
                next: $("html").attr("dir") == "rtl" ? "&larr;" : "&rarr;",
                previous: $("html").attr("dir") == "rtl" ? "&rarr;" : "&larr;",
            },
        },
    });

    // Basic datatable
    tableProject = $(".datatable-basic").DataTable();

    
});

function change() {
    tableProject.draw();
}
