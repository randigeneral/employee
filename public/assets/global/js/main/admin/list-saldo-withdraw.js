var tableProject = null;

$(document).ready(function() {
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        order: [],
        columnDefs: [
            {
                // orderable: true,
                width: 100,
                // order: [[0, "desc"]],
                "targets"  : 'no-sort',
                "orderable": false,
            },
        ],
        dom:
            '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: "",
            searchPlaceholder: "Type to filter...",
            lengthMenu: "<span>Show:</span> _MENU_",
            paginate: {
                first: "First",
                last: "Last",
                next: $("html").attr("dir") == "rtl" ? "&larr;" : "&rarr;",
                previous: $("html").attr("dir") == "rtl" ? "&rarr;" : "&larr;",
            },
        },
    });


    // Basic datatable
    tableProject = $("#mytable").DataTable({
        processing: true,
        serverSide: false,
        filter: true,
        orderMulti: true,
        bLengthChange: false,
        scrollX: true,

        ajax: {
            url: baseurl + "/admin-console/saldo/pencarian/list/data",
            dataType: "json",
            type: "GET",
            data: function (d) {
                // d.loc_id = $('#loc_id').val();
            },
        },
        columns: [
            {
                data: "user_withdraw",
                name: "user_withdraw",
                render: function (value, type, row) {
                    return '<span class="font-weight-bold">'+value+'</span>';
                }
            },
            {
                data: "created_at",
                name: "created_at",
            },
            {
                data: "request_amount",
                name: "request_amount",
                render: function (value, type, row) {
                    return '<span class="font-weight-bold">Rp. '+format_no_money(value)+'</span>';
                }
            },
            {
                data: "bank_account",
                name: "bank_account",
                render: function (value, type, row) {
                    return '<span class="font-weight-bold">'+value+'</span><br><span class="font-weight-light">a/n '+row['owner_name']+'</span>';
                },
            },
            {
                data: "bank_provider",
                name: "bank_provider",
            },
            {
                data: "status_descr",
                name: "status_descr",
                render: function (value, type, row) {
                    let status = '';
                    switch (row['status']) {
                        case 'WD01':
                            status = '<span class="badge badge-warning">'+value+'</span>';
                            break;
                        case 'WD02':
                            status = '<span class="badge badge-info">'+value+'</span>';
                            break;
                        case 'WD03':
                            status = '<span class="badge badge-success">'+value+'</span>';
                            break;
                        default:
                            status = '<span class="badge badge-danger">'+value+'</span>';
                            break;
                    }
                    return status;
                },
            },
            {
                data: "action",
                name: "action",
            },
        ],
    });

    // $("#mytable").on("click", "tbody tr", function () {
    //     var value = tableProject.row($(this)).data()["cat_id"];
    //     link = baseurl + "/admin-console/category/detail/" + value;
    //     window.location.href = link;
    // });

    $("#mytable").on("click", ".btn-delete", function () {
        // $("#delete-form")[0].reset();
        $("#deletepop").modal("show");
        $("input[name='brand_id']").val($(this).data("brand_id"));
        $("#brand-name").html($(this).data("brand_name"));
    });

    $(".confirm-delete-data").on("click", function () {
        deleteData();
    });

    function deleteData() {
        var fd = $("#delete-form").serializeArray();
        var apiurl = baseurl + "/admin-console/brand/delete";
        $.ajax({
            url: apiurl,
            type: "post",
            data: fd,
            beforeSend: function () {
                $("#deletepop").block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: "#fff",
                        opacity: 0.8,
                        cursor: "wait",
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: "none",
                    },
                });
            },
            success: function (json) {
                $("#deletepop").unblock();
                if (json.response.status == true) {
                    new PNotify({
                        title: "success!",
                        text: json.response.message,
                        addclass: "bg-success",
                    });
                    window.location.href =
                        baseurl + "/admin-console/brand/lists";
                } else {
                    new PNotify({
                        title: "failed!",
                        text: json.response.message,
                        addclass: "bg-danger",
                    });
                }
            },
            error: function (xhr) {
                new PNotify({
                    title: "Failed!",
                    text: xhr,
                    addclass: "bg-danger",
                });
                $("#deletepop").unblock();
            },
        });
    }
});

function change() {
    tableProject.draw();
}


function format_no_money(number) {
    if (number !== null) {
        var parts = number.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }
}

function unformat_no_money(number) {
    if (number !== null) {
        var parts = number.toString().split(",");
        return parts.join("");
    }
}

function changeStatus(saldo_request_id, status, pesan)
{
    if (status == 'WD04') {
        swal({
            title: 'Berikan alasan untuk proses pencairan gagal',
            input: 'textarea',
            inputPlaceholder: 'Masukan alasan disini',
            showCancelButton: true,
            inputClass: 'form-control',
            confirmButtonClass: 'btn btn-sm btn-info',
            cancelButtonClass: 'btn btn-sm btn-danger m-l-20',
            buttonsStyling: false
        }).then(function(result) {
            if(result.value) {
                $.ajax({
                    type: "POST",
                    url: baseurl + "/api/user/saldo/withdraw/action",
                    data: {
                        'saldo_request_id' : saldo_request_id,
                        'status' : status,
                        'message_error':result.value
                    },
                    headers: {
                        'token':Cookies.get('ACCESS_TOKEN'),
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function() {
                        // $('#main-form').block({
                        //     message: '<i class="icon-spinner4 spinner"></i>',
                        //     overlayCSS: {
                        //         backgroundColor: '#fff',
                        //         opacity: 0.8,
                        //         cursor: 'wait'
                        //     },
                        //     css: {
                        //         border: 0,
                        //         padding: 0,
                        //         backgroundColor: 'none'
                        //     }
                        // });
                    },
                    success: function(data) {
                        if ((data.response.status) && (data.response.code == 200)) {
                            swal({
                                title: 'Berhasil',
                                text: data.response.message,
                                type: 'success',
                                timer: 3000,
                                showConfirmButton: false
                            }).then(function(result) {
                                if (result.dismiss === swal.DismissReason.timer) {
                                    window.location.reload();
                                }
                            });
                        } else {
                            swal({
                                title: 'Terjadi kesalahan',
                                text: data.response.message,
                                type: 'error',
                                timer: 3000,
                                showConfirmButton: false,
                            }).then((result) => {
                                if (result.dismiss === swal.DismissReason.timer) {
                                    window.location.reload();
                                }
                            });
                        }
                        $('#main-form').unblock();
                    },
                    error: function(xhr) {
                        swal({
                            title: 'Terjadi kesalahan',
                            text: xhr.responseJSON.response.message,
                            type: 'error',
                            timer: 3000,
                            showConfirmButton: false,
                        }).then((result) => {
                            if (result.dismiss === swal.DismissReason.timer) {
                                window.location.reload();
                            }
                        });
                        // $('#main-form').unblock();
                    },
                    dataType: 'json'
                });
            }
        });
    }else{
        swal({
            title: pesan+" ?",
            text: "pastikan anda telah melakukan proses sesuai dengan data pencairan yang diajukan",
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Confirm',
            confirmButtonClass: 'btn btn-sm btn-info',
            cancelButtonClass: 'btn btn-sm btn-danger m-l-20',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: baseurl + "/api/user/saldo/withdraw/action",
                    data: {
                        'saldo_request_id' : saldo_request_id,
                        'status' : status
                    },
                    headers: {
                        'token':Cookies.get('ACCESS_TOKEN'),
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function() {
                        // $('#main-form').block({
                        //     message: '<i class="icon-spinner4 spinner"></i>',
                        //     overlayCSS: {
                        //         backgroundColor: '#fff',
                        //         opacity: 0.8,
                        //         cursor: 'wait'
                        //     },
                        //     css: {
                        //         border: 0,
                        //         padding: 0,
                        //         backgroundColor: 'none'
                        //     }
                        // });
                    },
                    success: function(data) {
                        if ((data.response.status) && (data.response.code == 200)) {
                            swal({
                                title: 'Berhasil',
                                text: data.response.message,
                                type: 'success',
                                timer: 3000,
                                showConfirmButton: false,
                            }).then((result) => {
                                if (result.dismiss === swal.DismissReason.timer) {
                                    window.location.reload();
                                }
                            });
                        } else {
                            nswal({
                                title: 'Terjadi kesalahan',
                                text: data.response.message,
                                type: 'error',
                                timer: 3000,
                                showConfirmButton: false,
                            }).then((result) => {
                                if (result.dismiss === swal.DismissReason.timer) {
                                    window.location.reload();
                                }
                            });
                        }
                        $('#main-form').unblock();
                    },
                    error: function(xhr) {
                        swal({
                            title: 'Terjadi kesalahan',
                            text: xhr.responseJSON.response.message,
                            type: 'error',
                            timer: 3000,
                            showConfirmButton: false,
                        }).then((result) => {
                            if (result.dismiss === swal.DismissReason.timer) {
                                window.location.reload();
                            }
                        });
                    },
                    dataType: 'json'
                });
            }
        });
    }

}
