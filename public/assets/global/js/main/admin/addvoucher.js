var image = null;

jQuery(document).ready(function ($) {

    $(".voucher_type").change(function() {
        let type = $(this).val();
        var row = ""
        if(type == '0') {
            row += '<label class="col-form-label col-lg-2">Nominal Voucher<span class="text-danger">*</span></label>'+
                '<div class="col-lg-10">'+
                '<div class="input-group">'+
                    '<span class="input-group-prepend">'+
                        '<span class="input-group-text">Rp</span>'+
                    '</span>'+
                    '<input type="number" class="form-control" name="voucher_amount" placeholder="E.g 20000" required>'+
                '</div>'+                                  
                '</div>';
        } else if(type == '1') {
            row += '<label class="col-form-label col-lg-2">Persentase Voucher<span class="text-danger">*</span></label>'+
                '<div class="col-lg-10">'+
                '<div class="input-group">'+
                    '<input type="number" class="form-control" name="voucher_percent" placeholder="E.g 30" required>'+
                    '<span class="input-group-append">'+
                        '<span class="input-group-text">%</span>'+
                    '</span>'+
                '</div>'+
                '</div>';
        }
         $('#forminput-voucher-type').html(row);
    });

    $('#voucher_start_at').daterangepicker({
        singleDatePicker: true,
        autoUpdateInput: false,
        locale: {
            format: 'YYYY-MM-DD'
        }
    }).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD'));
        setFinishDate()
    }).on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    
});

function setFinishDate() {
    $('#voucher_expired_at').daterangepicker({
        minDate: moment($('#voucher_start_at').val(), "YYYY-MM-DD"),
        singleDatePicker: true,
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear',
            format: 'YYYY-MM-DD'
        },
    }).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD'));

    }).on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
}

function getBase64(file) {
    var reader = new FileReader();
    reader.readAsDataURL(file.files[0]);
    reader.onload = function () {
        image = reader.result.replace(/^[^,]*,/, "");
    };
    reader.onerror = function (error) {};
}

$("#main-form").on("submit", function (e) {
    // check if the input is valid
    e.preventDefault();
    if (!$(this).valid()) return false;

    var fd = $("#main-form").serializeArray();
    fd.push({ name: "voucher_image", value: image });

    var apiurl = baseurl + "/admin-console/voucher/add";
    if ($("#main-form").valid()) {
        $.ajax({
            url: apiurl,
            type: "post",
            data: fd,
            beforeSend: function () {
                $(".content").block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: "#fff",
                        opacity: 0.8,
                        cursor: "wait",
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: "none",
                    },
                });
            },
            success: function (json) {
                $(".content").unblock();
                if (json.response.status == true) {
                    new PNotify({
                        title: "success!",
                        text: json.response.message,
                        addclass: "bg-success",
                    });
                    window.location.href =
                        baseurl + "/admin-console/voucher/lists";
                } else {
                    new PNotify({
                        title: "failed!",
                        text: json.response.message,
                        addclass: "bg-danger",
                    });
                }
            },
            error: function (xhr) {
                new PNotify({
                    title: "Failed!",
                    text: "Error something new",
                    addclass: "bg-danger",
                });
                $(".content").unblock();
            },
        });
    } else {
        new PNotify({
            title: "Validasi Error!",
            text: "Isian data masih ada yang kurang, silahkan perbaiki.",
            addclass: "bg-danger",
        });
    }
});
