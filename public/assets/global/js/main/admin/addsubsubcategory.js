var tableProject = null;

jQuery(document).ready(function ($) {
    //Select chooce category
    $("#category_id").select2();
    $("#subcategory_id").select2();

    $("#category_id").change(function() {
        let category_id = $(this).val();
        $("#subcategory_id").select2({
            placeholder: "-- Pilih Sub Kategori --",
            width: "100%",
            // dropdownParent: $("#responsive-modal"),
            ajax: {
                url: baseurl + "/admin-console/subsubcategory/listsubcategory/" + category_id,
                processResults: function(data) {
                    var dataList = $.map(data.data, function(obj) {
                        obj.id = obj.cat_id;
                        obj.text = obj.cat_name;
                        return obj;
                    });
                    return {
                        results: dataList,
                    };
                },
            },
        });
    });

    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        columnDefs: [
            {
                orderable: false,
                width: 100,
                order: [[0, "desc"]],
            },
        ],
        dom:
            '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: "",
            searchPlaceholder: "Type to filter...",
            lengthMenu: "<span>Show:</span> _MENU_",
            paginate: {
                first: "First",
                last: "Last",
                next: $("html").attr("dir") == "rtl" ? "&larr;" : "&rarr;",
                previous: $("html").attr("dir") == "rtl" ? "&rarr;" : "&larr;",
            },
        },
    });

    // Basic datatable
    tableProject = $("#mytable").DataTable({
        processing: true,
        serverSide: true,
        filter: true,
        orderMulti: true,
        bLengthChange: false,
        scrollX: true,

        ajax: {
            url: baseurl + "/admin-console/category/show",
            dataType: "json",
            type: "GET",
            data: function (d) {
                // d.loc_id = $('#loc_id').val();
                // d.job_type = $('#job_type').val();
                // d.job_start = $('#job_start').val();
                // d.job_end = $('#job_end').val();
            },
        },
        columns: [
            {
                data: "cat_image",
                name: "cat_image",
            },
            {
                data: "cat_name",
                name: "cat_name",
            },
            {
                data: "cat_descr",
                name: "cat_descr",
            },
            {
                data: "cat_meta_descr",
                name: "cat_meta_descr",
            },
            {
                data: "cat_meta_keyword",
                name: "cat_meta_keyword",
            },
            {
                data: "status",
                name: "status",
            },
        ],
    });

    $("#mytable").on("click", "tbody tr", function () {
        var value = tableProject.row($(this)).data()["user_id"];
        link = baseurl + "/admin-console/category/detail/" + value;
        window.location.href = link;
    });
});

function change() {
    tableProject.draw();
}

var image = null;

$("input[name=cat_image]").on("change", function () {
    getBase64(this, "cat_image");
});

function getBase64(file, target) {
    var reader = new FileReader();
    reader.readAsDataURL(file.files[0]);
    reader.onload = function () {
        if (target == "cat_image") {
            image = reader.result.replace(/^[^,]*,/, "");
        }
    };
    reader.onerror = function (error) {};
}

$("#main-form").on("submit", function (e) {
    // check if the input is valid
    e.preventDefault();
    if (!$(this).valid()) return false;

    var fd = $("#main-form").serializeArray();
    fd.push({ name: "cat_image", value: image });

    var apiurl = baseurl + "/admin-console/subsubcategory/add";
    if ($("#main-form").valid()) {
        $.ajax({
            url: apiurl,
            type: "post",
            data: fd,
            beforeSend: function () {
                $(".content").block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: "#fff",
                        opacity: 0.8,
                        cursor: "wait",
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: "none",
                    },
                });
            },
            success: function (json) {
                $(".content").unblock();
                if (json.response.status == true) {
                    new PNotify({
                        title: "success!",
                        text: json.response.message,
                        addclass: "bg-success",
                    });
                    window.location.href =
                        baseurl + "/admin-console/subsubcategory/lists";
                } else {
                    new PNotify({
                        title: "failed!",
                        text: json.response.message,
                        addclass: "bg-danger",
                    });
                }
            },
            error: function (xhr) {
                new PNotify({
                    title: "Failed!",
                    text: xhr,
                    addclass: "bg-danger",
                });
                $(".content").unblock();
            },
        });
    } else {
        new PNotify({
            title: "Validasi Error!",
            text: "Isian data masih ada yang kurang, silahkan perbaiki.",
            addclass: "bg-danger",
        });
    }
});
