$(document).on("click",".btn-delete",function() {
    var id = $(this).data('id')
    var nama = $(this).data('nama')
    if (confirm("Apakah kamu yakin menghapus data "+nama+" ?")) {
        deleteData(id);
    }
});

function deleteData(id){
    $.ajax({
        type: "POST",
        url: baseurl + "/api/employee/delete",
        type: "post",
        data: {'id':id},
        headers: {
            token: Cookies.get("ACCESS_TOKEN"),
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        dataType: "json",
        beforeSend: function () {
            block_page("Data sedang di proses . . ");
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (data) {
            console.log("success");
            console.log(data);
            if (data.response.code == 200 && data.response.status == true) {
                console.log("success");
                window.location.href = baseurl + '/employee';
            } else {
                alert(data.response.message);
            }
        },
        error: function (xhr) {
            alert(xhr.responseText);
        },
    });
}