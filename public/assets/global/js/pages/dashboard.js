var GooglePieBasic = function() {
    // Pie chart
    var _googlePieBasic = function() {
        if (typeof google == 'undefined') {
            console.warn('Warning - Google Charts library is not loaded.');
            return;
        }

        // Initialize chart
        google.charts.load('current', {
            callback: function () {

                // Draw chart
                drawPie();

                // Resize on sidebar width change
                $(document).on('click', '.sidebar-control', drawPie);

                // Resize on window resize
                var resizePieBasic;
                $(window).on('resize', function() {
                    clearTimeout(resizePieBasic);
                    resizePieBasic = setTimeout(function () {
                        drawPie();
                    }, 200);
                });
            },
            packages: ['corechart']
        });

        // Chart settings    
        function drawPie() {

            $.ajax({
                url: baseurl + "/api/employee/piechart",
                dataType: "json",
                type: "GET",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var dataGender = [['Jenis Kelamin', 'Total']];    // Define an array and assign columns for the chart.
    
                    // Loop through each data and populate the array.
                    $.each(data.data, function (index, value) {
                        var gender = ""
                        if(value.gender == 'L'){
                            gender = "Laki-laki";
                        } else if(value.gender == 'P'){
                            gender = "Perempuan"
                        } else {
                            gender = "-"
                        }
                        dataGender.push([gender, value.jumlah]);
                    });

                    var data_pie = new google.visualization.arrayToDataTable(dataGender);

                    // Define charts element
                    var pie_chart_element = document.getElementById('google-pie');

                    // Options
                    var options_pie = {
                        fontName: 'Roboto',
                        height: 300,
                        width: 500,
                        chartArea: {
                            left: 50,
                            width: '90%',
                            height: '90%'
                        }
                    };

                    // Instantiate and draw our chart, passing in some options.
                    var pie = new google.visualization.PieChart(pie_chart_element);
                    pie.draw(data_pie, options_pie);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(XMLHttpRequest.responseText);
                }
            });

        }
    };

    return {
        init: function() {
            _googlePieBasic();
        }
    }
}();
GooglePieBasic.init();


var GoogleColumnBasic = function() {
    // Column chart
    var _googleColumnBasic = function() {
        if (typeof google == 'undefined') {
            console.warn('Warning - Google Charts library is not loaded.');
            return;
        }

        // Initialize chart
        google.charts.load('current', {
            callback: function () {

                // Draw chart
                drawColumn();

                // Resize on sidebar width change
                $(document).on('click', '.sidebar-control', drawColumn);

                // Resize on window resize
                var resizeColumn;
                $(window).on('resize', function() {
                    clearTimeout(resizeColumn);
                    resizeColumn = setTimeout(function () {
                        drawColumn();
                    }, 200);
                });
            },
            packages: ['corechart']
        });

        // Chart settings
        function drawColumn() {

            $.ajax({
                url: baseurl + "/api/employee/barchart",
                dataType: "json",
                type: "GET",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var dataUmur = [['Rentang Umur', 'Jumlah']];    // Define an array and assign columns for the chart.
    
                    // Loop through each data and populate the array.
                    $.each(data.data, function (index, value) {
                        dataUmur.push([value.range_umur, value.jumlah]);
                    });

                    var data_bar = new google.visualization.arrayToDataTable(dataUmur);

                    // Define charts element
                    var line_chart_element = document.getElementById('google-column');

                    // Options
                    var options_column = {
                        fontName: 'Roboto',
                        height: 400,
                        fontSize: 12,
                        chartArea: {
                            left: '5%',
                            width: '94%',
                            height: 350
                        },
                        tooltip: {
                            textStyle: {
                                fontName: 'Roboto',
                                fontSize: 13
                            }
                        },
                        vAxis: {
                            title: 'Jumlah',
                            titleTextStyle: {
                                fontSize: 13,
                                italic: false
                            },
                            gridlines:{
                                color: '#e5e5e5',
                                count: 10
                            },
                            minValue: 0
                        },
                        // hAxis: {
                        //     title: 'Rentang Umur',
                        //     titleTextStyle: {
                        //         fontSize: 13,
                        //         italic: false
                        //     },
                        //     gridlines:{
                        //         color: '#e5e5e5',
                        //         count: 10
                        //     },
                        //     minValue: 0
                        // },
                        // legend: {
                        //     position: 'top',
                        //     alignment: 'center',
                        //     textStyle: {
                        //         fontSize: 12
                        //     }
                        // }
                    };

                    // Draw chart
                    var column = new google.visualization.ColumnChart(line_chart_element);
                    column.draw(data_bar, options_column);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(XMLHttpRequest.responseText);
                }
            });

            

            // Data
            // var data = google.visualization.arrayToDataTable([
            //     ['Year', 'Sales', 'Expenses'],
            //     ['2004',  1000,      400],
            //     ['2005',  1170,      460],
            //     ['2006',  660,       1120],
            //     ['2007',  1030,      540]
            // ]);


        }
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function() {
            _googleColumnBasic();
        }
    }
}();


// Initialize module
// ------------------------------

GoogleColumnBasic.init();














var GoogleBarBasic = function() {
    // Bar chart
    var _googleBarBasic = function() {
        if (typeof google == 'undefined') {
            console.warn('Warning - Google Charts library is not loaded.');
            return;
        }

        // Initialize chart
        google.charts.load('current', {
            callback: function () {

                // Draw chart
                drawBar();

                // Resize on sidebar width change
                $(document).on('click', '.sidebar-control', drawBar);

                // Resize on window resize
                var resizeBarBasic;
                $(window).on('resize', function() {
                    clearTimeout(resizeBarBasic);
                    resizeBarBasic = setTimeout(function () {
                        drawBar();
                    }, 200);
                });
            },
            packages: ['corechart']
        });

        // Chart settings
        function drawBar() {

            $.ajax({
                url: baseurl + "/api/employee/barchart",
                dataType: "json",
                type: "GET",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var dataUmur = [['Rentang Umur', 'Total']];    // Define an array and assign columns for the chart.
    
                    // Loop through each data and populate the array.
                    $.each(data.data, function (index, value) {
                        dataUmur.push([value.range_umur, value.jumlah]);
                    });

                    // var data_bar = new google.visualization.arrayToDataTable(dataUmur);
                    var data_bar = google.visualization.arrayToDataTable([
                        ['Year', 'Sales', 'Expenses'],
                        ['2004',  1000,      400],
                        ['2005',  1170,      460],
                        ['2006',  660,       1120],
                        ['2007',  1030,      540]
                    ]);

                    // Define charts element
                    var bar_chart_element = document.getElementById('google-bar');

                    // Options
                    var options_bar = {
                        fontName: 'Roboto',
                        height: 400,
                        fontSize: 12,
                        chartArea: {
                            left: '5%',
                            width: '94%',
                            height: 350
                        },
                        tooltip: {
                            textStyle: {
                                fontName: 'Roboto',
                                fontSize: 13
                            }
                        },
                        vAxis: {
                            gridlines:{
                                color: '#e5e5e5',
                                count: 10
                            },
                            minValue: 0
                        },
                        legend: {
                            position: 'top',
                            alignment: 'center',
                            textStyle: {
                                fontSize: 12
                            }
                        }
                    };

                    // Draw chart
                    var bar = new google.visualization.BarChart(bar_chart_element);
                    bar.draw(data_bar, options_bar);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(XMLHttpRequest.responseText);
                }
            });

            // Data
            // var dataBar = google.visualization.arrayToDataTable([
            //     ['Year', 'Sales'],
            //     ['2004',  1000],
            //     ['2005',  1170],
            //     ['2006',  660],
            //     ['2007',  1030]
            // ]);
        }
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function() {
            _googleBarBasic();
        }
    }
}();
GoogleBarBasic.init();