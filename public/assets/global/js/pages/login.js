$.beforesubmit = function () {
    $("#submit").prop('disabled', true);
    $("#submit").html(' <i class="icon-spinner spinner mr-2"></i> Loading...');
};

$.aftersubmit = function (btnsubmit) {
    $("#submit").prop('disabled', false);
    $("#submit").html(typeof btnsubmit !== "undefined" ? btnsubmit : "Submit <i class='icon-paperplane ml-2'></i>");
};

$("#form-login").on("submit", function(e) {
    e.preventDefault();
    var form = $(this);
    var post_url = form.attr("action");
    var request_method = form.attr("method");
    var post_data = new FormData(form[0]);
    var btnsubmit = "#btn-login";
    post_data.append("user_login_type", "manual");
    $.ajax({
        url: post_url,
        type: request_method,
        data: post_data,
        processData: false,
        contentType: false,
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        dataType: "JSON",
        beforeSend: function() {
            $.beforesubmit();
        },
        success: function(data) {
            if (data.response.code == 200) {
                console.log("success");
                console.log(data);
                Cookies.set("ACCESS_TOKEN", data.data.user_token);
                window.location.href = baseurl + '/dashboard';
            } else {
                alert(data.response.message);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.responseText);
        },
        complete: function() {
            $.aftersubmit();
        },
    });
});