$(document).ready(function(){
    var id = $('#employee_id').val();
    getData(id)
})

function getData(id){
    $.ajax({
        url: baseurl + "/api/employee/detail/"+id,
        headers: {
            token: Cookies.get("ACCESS_TOKEN"),
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        dataType: "json",
        beforeSend: function () {
            block_page("Data sedang di proses . . ");
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (data) {
            console.log("success");
            console.log(data);
            if (data.response.code == 200 && data.response.status == true) {
                var gender = '';
                if(data.data.gender == "L"){
                    gender = "Laki-laki";
                } else if(data.data.gender == "P"){
                    gender = "Perempuan";
                }

                $("#nama").html(data.data.nama);
                $("#nik").html(data.data.nik);
                $("#pekerjaan").html(data.data.job_title);
                $("#department").html(data.data.department);
                $("#alamat").html(data.data.alamat);
                $("#kota").html(data.data.kota);
                $("#gender").html(gender);
                $("#tanggal_lahir").html(getDateString(new Date(data.data.tanggal_lahir), "d M y"));
                $("#tanggal_masuk").html(getDateString(new Date(data.data.join_date), "d M y"));
            } else {
                alert(data.response.message);
            }
        },
        error: function (xhr) {
            alert(xhr.responseText);
        },
    });
}

