$(document).ready(function(){
    var id = $('#employee_id').val();
    getData(id)

    $("#form-add").on("submit", function (e) {
        e.preventDefault();
        if (!$(this).valid()) return false;

        $.ajax({
            type: "POST",
            url: baseurl + "/api/employee/save",
            data: $("#form-add").serializeArray(),
            headers: {
                token: Cookies.get("ACCESS_TOKEN"),
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            dataType: "json",
            beforeSend: function () {
                block_page("Data sedang di proses . . ");
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (data) {
                console.log("success");
                console.log(data);
                if (data.response.code == 200 && data.response.status == true) {
                    console.log("success");
                    window.location.href = baseurl + '/employee';
                } else {
                    alert(data.response.message);
                }
            },
            error: function (xhr) {
                alert(xhr.responseText);
            },
        });
    });
})

function getData(id){
    $.ajax({
        url: baseurl + "/api/employee/detail/"+id,
        headers: {
            token: Cookies.get("ACCESS_TOKEN"),
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        dataType: "json",
        beforeSend: function () {
            block_page("Data sedang di proses . . ");
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (data) {
            console.log("success");
            console.log(data);
            if (data.response.code == 200 && data.response.status == true) {
                var gender = '';
                if(data.data.gender == "L"){
                    gender = "Laki-laki";
                    $("#laki").attr("checked","checked");
                } else if(data.data.gender == "P"){
                    gender = "Perempuan";
                    $("#perempuan").attr("checked","checked");
                }

                $("#nama").val(data.data.nama);
                $("#nik").val(data.data.nik);
                $("#pekerjaan").val(data.data.job_title);
                $("#department").val(data.data.department);
                $("#alamat").val(data.data.alamat);
                $("#kota").val(data.data.kota);
                $("#tanggal_lahir").val(data.data.tanggal_lahir);
                $("#join_date").val(data.data.join_date.substring(0,10));
            } else {
                alert(data.response.message);
            }
        },
        error: function (xhr) {
            alert(xhr.responseText);
        },
    });
}

