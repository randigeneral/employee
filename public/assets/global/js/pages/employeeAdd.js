$(document).ready(function(){
   $("#form-add").on("submit", function (e) {
        e.preventDefault();
        if (!$(this).valid()) return false;

        $.ajax({
        type: "POST",
        url: baseurl + "/api/employee/save",
        type: "post",
        data: $("#form-add").serializeArray(),
        headers: {
            token: Cookies.get("ACCESS_TOKEN"),
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        dataType: "json",
        beforeSend: function () {
            block_page("Data sedang di proses . . ");
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (data) {
            console.log("success");
            console.log(data);
            if (data.response.code == 200 && data.response.status == true) {
                console.log("success");
                window.location.href = baseurl + '/employee';
            } else {
                alert(data.response.message);
            }
        },
        error: function (xhr) {
            alert(xhr.responseText);
        },
    });
    });
})
