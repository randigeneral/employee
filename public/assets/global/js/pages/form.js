$.beforesubmit = function () {
    $("#submit").prop('disabled', true);
    $("#submit").html(' <i class="icon-spinner spinner mr-2"></i> Loading...');

    $(".submit").prop('disabled', true);
    $(".submit").html(' <i class="icon-spinner spinner mr-2"></i> Loading...');
};

$.aftersubmit = function (btnsubmit) {
    $(".submit").prop('disabled', false);
    $(".submit").html(typeof btnsubmit !== "undefined" ? btnsubmit : "Submit <i class='icon-paperplane ml-2'></i>");

    $("#submit").prop('disabled', false);
    $("#submit").html(typeof btnsubmit !== "undefined" ? btnsubmit : "Submit <i class='icon-paperplane ml-2'></i>");
};

$.alertsuccess = function (text, redirect) {
    return swal({
        title: "Success!",
        text: text,
        type: "success",
        timer: 7000,
        showCancelButton: false,
        showConfirmButton: true,
    }).then(
        function () {
            if (typeof redirect !== "undefined")
                window.location.href = BASE_URL + redirect;
        },
        function (dismiss) {
            if (dismiss === "timer") {
            }
        }
    );
};

$.alerterror = function (text) {
    return swal({
        title: "Warning!",
        text: typeof text === "undefined" ? "Error something new" : text,
        type: "error",
        confirmButtonText: "Ok",
    });
};

$.submitform = function (paramsubmit) {

    let post_data =  typeof paramsubmit.post_data === "undefined" ? null : paramsubmit.post_data;
    let btnsubmit =  typeof paramsubmit.btnsubmit === "undefined" ? null : paramsubmit.btnsubmit;
    let post_url = typeof paramsubmit.post_url === "undefined" ? null : paramsubmit.post_url;
    let redirect = typeof paramsubmit.redirect === "undefined" ? null : paramsubmit.redirect;

    $.ajax({
        url: BASE_URL + post_url,
        type: "POST",
        data: post_data,
        dataType: "JSON",
        beforeSend: function () {
            $.beforesubmit();
        },
        success: function (data) {
            if (data.response.code == 200) {
                $.alertsuccess(data.response.message, redirect);
            }
        },
        error: function (xhr) {
            $.aftersubmit(btnsubmit);
            $.alerterror(xhr.responseJSON.response.message);
        },
        complete: function () {
            $.aftersubmit(btnsubmit);
        },
    });

};


function block_page(msg) {
    $.blockUI({
        message:
            '<i class="fas fa-spin fa-sync text-white"></i><br><span class="text-semibold text-white">' +
            msg +
            "</span>",
        timeout: 2000, //unblock after 2 seconds
        overlayCSS: {
            backgroundColor: "#000",
            opacity: 0.5,
            cursor: "wait",
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: "transparent",
            zIndex: 999999,
        },
        baseZ: 999999,
    });
}

var  getDateString = function(date, format) {
    if(date == "Invalid Date"){
        return "-";
    } else {
        var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
            getPaddedComp = function (comp) {
                return ((parseInt(comp) < 10) ? ('0' + comp) : comp)
            },
            formattedDate = format,
            o = {
                "y+": date.getFullYear(), // year
                "M+": months[date.getMonth()], //month
                "d+": getPaddedComp(date.getDate()), //day
                "h+": getPaddedComp((date.getHours() > 12) ? date.getHours() % 12 : date.getHours()), //hour
                "H+": getPaddedComp(date.getHours()), //hour
                "m+": getPaddedComp(date.getMinutes()), //minute
                "s+": getPaddedComp(date.getSeconds()), //second
                "S+": getPaddedComp(date.getMilliseconds()), //millisecond,
                "b+": (date.getHours() >= 12) ? 'PM' : 'AM'
            };

        for (var k in o) {
            if (new RegExp("(" + k + ")").test(format)) {
                formattedDate = formattedDate.replace(RegExp.$1, o[k]);
            }
        }
        return formattedDate;
    }
};