# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.4.17-MariaDB)
# Database: test
# Generation Time: 2021-05-23 17:28:52 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table m_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `m_user`;

CREATE TABLE `m_user` (
  `user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `user_firstname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_lastname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_gender` enum('L','P') COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_password` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_pin` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_pin_status` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `m_user` WRITE;
/*!40000 ALTER TABLE `m_user` DISABLE KEYS */;

INSERT INTO `m_user` (`user_id`, `user_email`, `user_firstname`, `user_lastname`, `user_gender`, `user_phone`, `user_image`, `user_password`, `user_pin`, `user_pin_status`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`)
VALUES
	('433b5598-bb5f-4be2-8faa-8b52e0e01a31','rmaizul@gmail.com','Randi','Maizul','L','081381626254',NULL,'$2y$10$/2rb/5fewvMhrbx0CK9Dj.QtnAEKH3bv1MKU1MKK3kKYU4axCXTrq',NULL,NULL,'A','433b5598-bb5f-4be2-8faa-8b52e0e01a31','2020-08-11 06:33:42','433b5598-bb5f-4be2-8faa-8b52e0e01a31','2020-08-11 10:03:23'),
	('dcd852e9-4779-42e8-ba9f-d60feb8b6cba','admin@gmail.com','Admin','','L','081381626254',NULL,'$2y$12$PU26mPiRHyiY1KBgzpm6kew/r9BsU1n/O6TdcJCe.ESCtsebrc13C',NULL,NULL,'A','dcd852e9-4779-42e8-ba9f-d60feb8b6cba','2020-10-21 12:21:35','dcd852e9-4779-42e8-ba9f-d60feb8b6cba','2020-10-21 12:22:15');

/*!40000 ALTER TABLE `m_user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table m_user_login
# ------------------------------------------------------------

DROP TABLE IF EXISTS `m_user_login`;

CREATE TABLE `m_user_login` (
  `user_login_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_login_type` enum('facebook','google','manual','phone') COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_token` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_token_expired` datetime DEFAULT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`user_login_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `m_user_login` WRITE;
/*!40000 ALTER TABLE `m_user_login` DISABLE KEYS */;

INSERT INTO `m_user_login` (`user_login_id`, `user_id`, `user_email`, `user_phone`, `user_login_type`, `user_token`, `user_token_expired`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`)
VALUES
	('065ba861-d73f-49ea-bbe0-0bf26ce23ccf','433b5598-bb5f-4be2-8faa-8b52e0e01a31','rmaizul@gmail.com',NULL,'manual','$2y$10$iavUaWBdyUn034qYRPsEjODXkcdXcBUH8QLalgo7DLXonDWcctST6','2021-05-30 01:56:02',NULL,'433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 03:16:02',NULL,NULL),
	('1570ae73-da0a-491d-b24f-64655490be8b','433b5598-bb5f-4be2-8faa-8b52e0e01a31','rmaizul@gmail.com',NULL,'manual','$2y$10$JxwJw8.AYD7DsO9MVhLbjOvW8IUyCEosB52PEBPKdTrOlSbRYGzq6','2021-05-30 02:04:38',NULL,'433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 03:24:38',NULL,NULL),
	('2586ccf4-4b4c-468d-aa79-c9f53074d8b2','433b5598-bb5f-4be2-8faa-8b52e0e01a31','rmaizul@gmail.com',NULL,'manual','$2y$10$OmeB5xEansd8pNl0uSQuKuzi/d8Nr76/w4hacb4TGpRwfgLHV4pm.','2021-05-30 01:13:53',NULL,'433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 02:33:53',NULL,NULL),
	('2f9050d5-9499-4759-9463-fac9732f5f92','dcd852e9-4779-42e8-ba9f-d60feb8b6cba','admin@gmail.com',NULL,'manual','$2y$10$v.onL7a7Ghx62284RQ3dmOP4YS5phvbOSRrVXuYNSX7YVhFWdR0Y.','2021-05-30 02:15:10',NULL,'dcd852e9-4779-42e8-ba9f-d60feb8b6cba','2021-05-23 03:35:10',NULL,NULL),
	('31a3fca9-c396-4fc2-a295-bbe9b29275cb','433b5598-bb5f-4be2-8faa-8b52e0e01a31','rmaizul@gmail.com',NULL,'manual','$2y$10$fsu6CGV0yygtP0c5QICmV.FMZtbMtM098AQ0n59HauMw/5cHSEC/G','2021-05-30 01:55:36',NULL,'433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 03:15:36',NULL,NULL),
	('4e421741-ebc1-458a-a668-cc45ed6d293f','433b5598-bb5f-4be2-8faa-8b52e0e01a31','rmaizul@gmail.com',NULL,'manual','$2y$10$ygDZbCsGh4qjYf6pzGjlSu0G7QHMufO4xWOfsjZuo4CrNSyiT60F6','2021-05-30 02:15:17',NULL,'433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 03:35:17',NULL,NULL),
	('50e4bbd8-b556-46e0-9338-11ffcaf24140','433b5598-bb5f-4be2-8faa-8b52e0e01a31','rmaizul@gmail.com',NULL,'manual','$2y$10$U1WWSICj4bgoq2WtcLh7GeCk/ldPg6m/LrSsxjKcZwE.0p5m1gGYy','2021-05-30 01:55:46',NULL,'433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 03:15:46',NULL,NULL),
	('573ad10b-a428-4859-a7a5-c6be0a2589af','433b5598-bb5f-4be2-8faa-8b52e0e01a31','rmaizul@gmail.com',NULL,'manual','$2y$10$KiHx0Az885Xrl9Zx/ULcO.hnDfyorNVwkDGUj5ho9gHygedAJX9zC','2021-05-30 01:58:50',NULL,'433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 03:18:50',NULL,NULL),
	('5de8b68f-6ff2-47c9-83a4-45d0af0f8762','433b5598-bb5f-4be2-8faa-8b52e0e01a31','rmaizul@gmail.com',NULL,'manual','$2y$10$tTmhtMfnwyYFNrnnzV.hxeMsLCw5FhKW40wZlqCEzPPajk0oK5ghu','2021-05-30 02:04:42',NULL,'433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 03:24:42',NULL,NULL),
	('659820bf-789b-425b-917f-d97413a52f72','433b5598-bb5f-4be2-8faa-8b52e0e01a31','rmaizul@gmail.com',NULL,'manual','$2y$10$eunFzffw/U8.KRMvQV57iOxQMOCBE9b1WU.cGxWHd3X56tj.3tU.6','2021-05-30 01:33:29',NULL,'433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 02:53:28',NULL,NULL),
	('6cd2e9f3-44dc-4a28-9caa-f1ff0b187206','433b5598-bb5f-4be2-8faa-8b52e0e01a31','rmaizul@gmail.com',NULL,'manual','$2y$10$dl0lWZZMf0mrOvBjzTjC9uxRqQ8w8PKd7ehvk.0D7p83xpMVcn.YS','2021-05-30 01:12:15',NULL,'433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 02:32:15',NULL,NULL),
	('8f649aac-22aa-41ac-8677-1ebbdaa6bc07','433b5598-bb5f-4be2-8faa-8b52e0e01a31','rmaizul@gmail.com',NULL,'manual','$2y$10$N4ykwtbMkWALq95n1lLMIOGnJuBhK3hhDTXcf4tA3RdTvmJkRela6','2021-05-30 01:47:37',NULL,'433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 03:07:36',NULL,NULL),
	('aa6a4dcd-bbcc-4372-ae11-aa687e564f47','433b5598-bb5f-4be2-8faa-8b52e0e01a31','rmaizul@gmail.com',NULL,'manual','$2y$10$8nfiD40QTYlJ0U0RFUavYeA2NYa0kX1MdoJDmRS1LK2tb78k/W7U2','2021-05-30 01:11:49',NULL,'433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 02:31:49',NULL,NULL),
	('b33b2dc8-6187-43be-bc8e-f35bb5a7496a','433b5598-bb5f-4be2-8faa-8b52e0e01a31','rmaizul@gmail.com',NULL,'manual','$2y$10$GKvrNUgxieJe2yMCYzpIw.bAT2lJ7JJkNhSkCCz1yfWWuCp9UqHsC','2021-05-30 01:24:19',NULL,'433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 02:44:19',NULL,NULL),
	('c0c08262-50a8-40c2-97be-2d7a94e49f43','433b5598-bb5f-4be2-8faa-8b52e0e01a31','rmaizul@gmail.com',NULL,'manual','$2y$10$1VdmmE8mc/AFbkijcOhj9eQiQiMNvoDVXe2/TeJHVeMCczvSOgY5i','2021-05-30 02:09:18',NULL,'433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 03:29:18',NULL,NULL),
	('c97e3a88-a6dc-4753-9aca-53ebbb5c9045','433b5598-bb5f-4be2-8faa-8b52e0e01a31','rmaizul@gmail.com',NULL,'manual','$2y$10$7/mbgljUTaeogOD/3ot8gOVVDowM0Wnx7CZobUAp5bipuuLkULawq','2021-05-30 01:47:24',NULL,'433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 03:07:24',NULL,NULL),
	('e6e5480e-3937-437b-b8f2-297b8548a321','433b5598-bb5f-4be2-8faa-8b52e0e01a31','rmaizul@gmail.com',NULL,'manual','$2y$10$sNGBmYXT/1uawgOVoNJiuOJjgYuPlVzs8jK97q/OAlCJKW9bvkFvq','2021-05-30 01:55:39',NULL,'433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 03:15:39',NULL,NULL),
	('e795ed91-f15f-4320-877d-73beee16451a','433b5598-bb5f-4be2-8faa-8b52e0e01a31','rmaizul@gmail.com',NULL,'manual','$2y$10$26dUbwZqzo.FtqV1jiy9uONyDAkKl8IzzVdHCxjMLI07oPvLhfiuS','2021-05-30 02:03:55',NULL,'433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 03:23:55',NULL,NULL),
	('e9582751-a001-4da6-9198-56fb7443e079','433b5598-bb5f-4be2-8faa-8b52e0e01a31','rmaizul@gmail.com',NULL,'manual','$2y$10$5wUoRcrZWNE0xNZ2IIqx0eGW1zicsFmY.MVmXDzKgPQNjMS3.12PS','2021-05-30 01:52:10',NULL,'433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 03:12:10',NULL,NULL),
	('f97e962f-964e-4af6-b00b-6a472bd342ab','dcd852e9-4779-42e8-ba9f-d60feb8b6cba','admin@gmail.com',NULL,'manual','$2y$10$mmIZHKXXOqRaDpZ1i9c5dOoTrSH2XNP1KaAxKkBCPlEts/edjjvGC','2021-05-30 12:43:21',NULL,'dcd852e9-4779-42e8-ba9f-d60feb8b6cba','2021-05-23 14:03:21',NULL,NULL),
	('fe35f73a-0ed3-4a2f-a14e-b8772c854f1e','433b5598-bb5f-4be2-8faa-8b52e0e01a31','rmaizul@gmail.com',NULL,'manual','$2y$10$9lP7a6xbcDug7l3bEoifFOhBtonIvWMq5Ywu4woqZD0TcR5.l3RBO','2021-05-30 01:10:41',NULL,'433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 02:30:41',NULL,NULL),
	('ff37cad4-1ff1-455c-8694-c4da759ed2e8','433b5598-bb5f-4be2-8faa-8b52e0e01a31','rmaizul@gmail.com',NULL,'manual','$2y$10$O2nHrTrqRxURfuTSSl6fxOQGoLKkw66iwZOYs8vpQLNxAqlcwOoJi','2021-05-30 01:04:29',NULL,'433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 02:24:29',NULL,NULL);

/*!40000 ALTER TABLE `m_user_login` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_employee
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_employee`;

CREATE TABLE `tbl_employee` (
  `id` varchar(50) NOT NULL DEFAULT '',
  `nik` varchar(50) DEFAULT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `gender` enum('P','L') DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `kota` varchar(50) DEFAULT NULL,
  `job_title` varchar(100) DEFAULT NULL,
  `department` varchar(100) DEFAULT NULL,
  `join_date` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT '',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_employee` WRITE;
/*!40000 ALTER TABLE `tbl_employee` DISABLE KEYS */;

INSERT INTO `tbl_employee` (`id`, `nik`, `nama`, `alamat`, `gender`, `tanggal_lahir`, `kota`, `job_title`, `department`, `join_date`, `created_at`, `created_by`, `updated_at`, `updated_by`)
VALUES
	('1','1306022904920003','Randi Maizul','Jalan Cimanuk Blok B1','L','1992-04-29','Bogor','Proggrammer','Budes & Legal','2021-05-21 00:00:00','2021-05-22 09:28:40',NULL,'2021-05-23 07:55:40','433b5598-bb5f-4be2-8faa-8b52e0e01a31'),
	('3750fd17-9f4a-4138-a410-36a01484a4f1','1306022904920002','Sisca Zulfa 2','Jl, Prambanan','P','1988-05-30','Padang','Proggrammer','Finance','2021-05-23 00:00:00','2021-05-23 07:07:32','433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 07:08:11','433b5598-bb5f-4be2-8faa-8b52e0e01a31'),
	('3750fd17-9f4a-4138-a410-36a01484a4f2','1306022904920002','Sisca Zulfa 3','Jl, Prambanan','P','1979-05-30','Padang','Proggrammer','Finance','2021-05-23 00:00:00','2021-05-23 07:07:32','433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 07:08:11','433b5598-bb5f-4be2-8faa-8b52e0e01a31'),
	('3750fd17-9f4a-4138-a410-36a01484a4f3','1306022904920002','Sisca Zulfa','Jl, Prambanan','P','2021-05-30','Padang','Proggrammer','Finance','2021-05-23 00:00:00','2021-05-23 07:07:32','433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 07:08:11','433b5598-bb5f-4be2-8faa-8b52e0e01a31'),
	('3750fd17-9f4a-4138-a410-36a01484a4f6','1306022904920002','Sisca Zulfa','Jl, Prambanan','P','1956-05-30','Padang','Proggrammer','Finance','2021-05-23 00:00:00','2021-05-23 07:07:32','433b5598-bb5f-4be2-8faa-8b52e0e01a31','2021-05-23 07:08:11','433b5598-bb5f-4be2-8faa-8b52e0e01a31');

/*!40000 ALTER TABLE `tbl_employee` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
