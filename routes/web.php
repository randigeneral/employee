<?php

use App\Http\Controllers\Auth\AuthController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\EmployeeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AuthController::class, 'login']);
Route::get('/login', [AuthController::class, 'login']);
Route::get('/logout', [AuthController::class, 'logout']);

Route::group(['middleware' => 'login'], function () {
    Route::get('/dashboard', [DashboardController::class, 'index']);

    Route::get('/employee', [EmployeeController::class, 'index']);
    Route::get('/employee/add', [EmployeeController::class, 'add']);
    Route::get('/employee/detail/{id}', [EmployeeController::class, 'detail']);
    Route::get('/employee/edit/{id}', [EmployeeController::class, 'edit']);
});

