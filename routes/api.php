<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ApiAuthController;
use App\Http\Controllers\Api\ApiEmployeeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/* authentication */
Route::post('/auth/login', [ApiAuthController::class, 'login']);

/* Employee */
Route::get('/employee/detail/{id}', [ApiEmployeeController::class, 'detail']);
Route::post('/employee/save', [ApiEmployeeController::class, 'save']);
Route::post('/employee/delete', [ApiEmployeeController::class, 'delete']);
Route::get('/employee/piechart', [ApiEmployeeController::class, 'piechart']);
Route::get('/employee/barchart', [ApiEmployeeController::class, 'barchart']);
