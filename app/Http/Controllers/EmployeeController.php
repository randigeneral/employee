<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Cookie;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->employee = new Employee();
    }

    public function index(){
        $data = [
            "employee" => $this->employee->allData()
        ];

        return view('employee/index',$data);
    }

    public function add(){
        return view('employee/add');
    }

    public function detail($id){
        $data['id'] = $id;
        return view('employee/detail',$data);
    }

    public function edit($id){
        $data['id'] = $id;
        return view('employee/edit',$data);
    }
}
