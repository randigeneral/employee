<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\UserLogin;
use Carbon\Carbon;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function checkToken($token)
    {
        $result = array();
        $login = UserLogin::join('m_user', 'm_user_login.user_id', '=', 'm_user.user_id')
                    ->select('m_user_login.*','m_user.user_password', 'm_user.user_pin')
                    ->where('user_token', $token)->first();
        if (empty($login)) {
            $result['tokenStatus'] = "invalid token";
            return $result;
        } elseif (Carbon::now() >= $login->user_token_expired) {
            $result['tokenStatus'] = "token expired";
            return $result;
        } else {
            return $login;
        }
    }
}
