<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function login(){
        $token = $this->checkToken(Cookie::get('ACCESS_TOKEN'));
        if (empty($token['tokenStatus'])) {
            return redirect('dashboard');
        } else {
            $data = array(
                'data' => null,
                'title' => 'Login ',
            );
            return view('auth.login', $data);
        }
    }

    public function logout()
    {
        session(['login' => null]);
        $cookie =  Cookie::forget('ACCESS_TOKEN');
        Session::flush();
        return redirect('login')->withCookie($cookie);
    }
}
