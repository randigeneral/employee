<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Helpers\Helper;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->employee = new Employee(); 
    }

    public function index(){
        $data = [
            "employee" => $this->employee->allData()
        ];

        return view('dashboard/index',$data);
    }
}
