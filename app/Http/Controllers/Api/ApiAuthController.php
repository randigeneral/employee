<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\UserLogin;

use App\Utils\Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cookie;

class ApiAuthController extends Controller
{

    public function __construct()
    {
        $this->user = new Users();
        $this->userLogin = new UserLogin();
    }

    public function login(Request $request)
    {
        $data = $request->all();
        $response = new Response;
        DB::beginTransaction();
        try {
            $rules = [
                'email' => 'required|email',
                'password'    => 'required',
            ];

            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                throw new \Exception($this->validationErrorsToString($validator->errors()));
            }

            $user = $this->user->findByEmail($data['email']);
            // echo $user->status; die();
            if (!$user) {
                // throw new \Exception('Email tidak ditemukan');
                $response->setStatus(false);
                $response->setCode(500);
                $response->setMessage("Email tidak ditemukan");
                return $response->sendResponse();
            } else if (!Hash::check($data['password'], $user->user_password)) {
                // throw new \Exception("Password yang anda masukkan salah");
                $response->setStatus(false);
                $response->setCode(500);
                $response->setMessage("Password yang anda masukkan salah");
                return $response->sendResponse();
            }

            #create log login 
            $access_token = Hash::make($data['email'] . rand() . Carbon::now()->toDateTimeString());
            $userdatalogin['created_by'] = $data['email'];
            $userdatalogin['created_at'] = Carbon::now()->toDateTimeString();
            $userdatalogin['user_token'] = $access_token;
            $userdatalogin['user_token_expired'] = Carbon::now()->addMinutes(10000)->toDateTimeString();
            $userdatalogin['user_login_id'] = \Ramsey\Uuid\Uuid::uuid4();
            $userdatalogin['user_id'] = $user->user_id;
            $userdatalogin['user_email'] = $user->user_email;
            $userdatalogin['created_by'] = $user->user_id;
            $userdatalogin['user_login_type'] = 'manual';
            $this->userLogin->create($userdatalogin);

            #set session
            $session = $request->session();
            $session->put('user_id', $user->user_id);
            $session->put('user_name', $user->user_firstname.' '.$user->user_lastname);
            $session->put('email', $user->user_email);
            $session->put('token', $access_token);
            $session->put('login', TRUE);

            # get data user
            $profile = $this->user->findByEmail($data['email']);
            $userdatalogin['user_firstname'] = $profile->user_firstname;
            $userdatalogin['user_lastname'] = $profile->user_lastname;
            $userdatalogin['user_phone'] = $profile->user_phone;
            $userdatalogin['user_gender'] = $profile->user_phone;

            $response->setStatus(true);
            $response->setCode(200);
            $response->setData($userdatalogin);
            $response->setMessage("Login success");

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $response->setMessage($e->getMessage());
        }

        return $response->sendResponse();
    }
}
