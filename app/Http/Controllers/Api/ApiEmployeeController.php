<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Utils\Response;
use Carbon\Carbon;
use App\Models\Employee;

class ApiEmployeeController extends Controller
{
    public function __construct()
    {
        $this->employee = new Employee();
    }

    public function detail($id)
    {
        $employee = $this->employee->getData($id);
        $response = new Response;

        if($employee){
            $response->setStatus(true);
            $response->setCode(200);
            $response->setData($employee);
            $response->setMessage("Data berhasil diambil");
        } else {
            $response->setStatus(false);
            $response->setCode(200);
            $response->setMessage("Data tidak ada");
        }

        return $response->sendResponse();
    }
    
    public function save(Request $request)
    {
        $employee = $this->employee->saveData($request->all());
        $response = new Response;

        if($employee['success']){
            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage($employee['msg']);
        } else {
            $response->setStatus(false);
            $response->setCode(200);
            $response->setMessage($employee['msg']);
        }

        return $response->sendResponse();
    }
    
    public function delete(Request $request)
    {
        $employee = $this->employee->deleteData($request->all());
        $response = new Response;

        if($employee['success']){
            $response->setStatus(true);
            $response->setCode(200);
            $response->setMessage($employee['msg']);
        } else {
            $response->setStatus(false);
            $response->setCode(200);
            $response->setMessage($employee['msg']);
        }

        return $response->sendResponse();
    }

    public function piechart(){
        $pie = $this->employee->pieChart();

        $response = new Response;

        if($pie){
            $response->setStatus(true);
            $response->setCode(200);
            $response->setData($pie);
            $response->setMessage('Berhasil');
        } else {
            $response->setStatus(false);
            $response->setCode(200);
            $response->setMessage('Gagal');
        }

        return $response->sendResponse();
    }

    public function barchart(){
        $bar = $this->employee->barChart();

        $response = new Response;

        if($bar){
            $response->setStatus(true);
            $response->setCode(200);
            $response->setData($bar);
            $response->setMessage('Berhasil');
        } else {
            $response->setStatus(false);
            $response->setCode(200);
            $response->setMessage('Gagal');
        }

        return $response->sendResponse();
    }
}
