<?php

namespace App\Helpers;

use App\Models\UserLogin;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Cookie;
use Carbon\Carbon;


class Helper {

    public static function getActiveUser()
    {
        $login = UserLogin::join('m_user', 'm_user_login.user_id', '=', 'm_user.user_id')
                    ->where('m_user_login.user_token', Cookie::get('ACCESS_TOKEN'))
                    ->select([
                        'm_user.user_email', 'm_user.user_firstname', 'm_user.user_lastname', 'm_user.user_gender', 'm_user.user_phone', 'm_user.user_image', 'm_user.user_id', 'm_user_login.user_token_expired', 'm_user.user_pin', 'm_user.user_pin_status'
                    ])->first();
        return ((empty($login)) || (Carbon::now() >= $login->user_token_expired)) ?  false :  $login;
    }

    public static function tgl_indo($tgl)
	{
		$tanggal = substr($tgl,8,2);
		$bulan = Helper::get_bulan(substr($tgl,5,2));
		$tahun = substr($tgl,0,4);
		return $tanggal.' '.$bulan.' '.$tahun;
	}

	public static function get_bulan($bln)
	{
		switch ($bln) {
				case 1 :
				return "Januari";
				break;
				case 2 :
				return "Februari";
				break;
				case 3 :
				return "Maret";
				break;
				case 4 :
				return "April";
				break;
				case 5 :
				return "Mei";
				break;
				case 6 :
				return "Juni";
				break;
				case 7 :
				return "Juli";
				break;
				case 8 :
				return "Agustus";
				break;
				case 9 :
				return "September";
				break;
				case 10 :
				return "Oktober";
				break;
				case 11 :
				return "November";
				break;
				case 12 :
				return "Desember";
				break;
			}
	}

	public static function nameday($tgl)
	{
		$tgls=explode("-", $tgl);
		$namahari= date("l", mktime(0, 0, 0, $tgls[1], $tgls[2], $tgls[0]));
		switch ($namahari)
				{
				case "Monday" :
				return "Senin";
				break;
				case "Tuesday" :
				return "Selasa";
				break;
				case "Wednesday" :
				return "Rabu";
				break;
				case "Thursday" :
				return "Kamis";
				break;
				case "Friday" :
				return "Jumat";
				break;
				case "Saturday" :
				return "Sabtu";
				break;
				case "Sunday" :
				return "Minggu";
				break;
				}
	}

	public static function rp($int)
	{
		return "Rp, ".number_format($int, 0, '', '.');
	}

	public static function rp_nologo($int)
	{
		return number_format($int, 0, '', '.');
    }

}
