<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserLogin extends Model
{

    protected $table = 'm_user_login';
    protected $primaryKey = 'user_login_id';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'user_login_id'
        ,'user_id'
        ,'user_email'
        ,'user_phone'
        ,'user_login_type'
        ,'user_token'
        ,'user_token_expired'
        ,'status'
        ,'created_by'
        ,'updated_by'
        ,'created_at'
        ,'updated_at'
    ];

    public function create($data){
        return DB::table('m_user_login')->insert($data);
    }

}
