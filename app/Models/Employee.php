<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Utils\Response;

class Employee extends Model
{
    public function allData(){
        return DB::table('tbl_employee')->get();
    }

    public function getData($id){
        return DB::table('tbl_employee')->where('id',$id)->first();
    }

    public function saveData($request)
    {
        $response = new Response;
		DB::beginTransaction();
        try {
            if(!empty($request['id'])){
                $data['nama'] = $request['nama'];
                $data['nik'] = $request['nik'];
                $data['alamat'] = $request['alamat'];
                $data['gender'] = $request['jenis_kelamin'];
                $data['tanggal_lahir'] = $request['tanggal_lahir'];
                $data['kota'] = $request['kota'];
                $data['job_title'] = $request['pekerjaan'];
                $data['department'] = $request['department'];
                $data['join_date'] = $request['join_date'];
                $data['updated_by'] = session('user_id');
                $data['updated_at'] = Carbon::now()->toDateTimeString();
                DB::table('tbl_employee')->where('id',$request['id'])->update($data);
	        } else {
                $data['id'] = \Ramsey\Uuid\Uuid::uuid4();
                $data['nama'] = $request['nama'];
                $data['nik'] = $request['nik'];
                $data['alamat'] = $request['alamat'];
                $data['gender'] = $request['jenis_kelamin'];
                $data['tanggal_lahir'] = $request['tanggal_lahir'];
                $data['kota'] = $request['kota'];
                $data['job_title'] = $request['pekerjaan'];
                $data['department'] = $request['department'];
                $data['join_date'] = $request['join_date'];
                $data['created_by'] = session('user_id');
                $data['created_at'] = Carbon::now()->toDateTimeString();
                DB::table('tbl_employee')->insert($data);
	        }

           
            $array = array('success'=>true, 'msg'=>'Save product success');
           
            DB::commit();
		}
        catch(\Exception $e){
            $array = array('success'=>false, 'msg'=>$e->getMessage());
            DB::rollback();
        }

        return $array;
    }

    public function deleteData($request){
        DB::beginTransaction();
        try {
            DB::table('tbl_employee')->where('id',$request['id'])->delete();
            $array = array('success'=>true, 'msg'=>'Save product success');
           
            DB::commit();
		}
        catch(\Exception $e){
            $array = array('success'=>false, 'msg'=>$e->getMessage());
            DB::rollback();
        }

        return $array;
    }

    public function pieChart(){
        $pie = DB::table('tbl_employee')
                    ->select(DB::raw('gender, count(id) as jumlah'))
                    ->groupBy('gender')
                    ->get();

        return $pie;

    }
    
    public function barChart(){
        $bar = DB::select(DB::raw("SELECT
            CASE
                WHEN umur < 30 THEN ' < 30'
                WHEN umur BETWEEN 30 and 40 THEN '30 - 40'
                WHEN umur BETWEEN 40 and 50 THEN '40 - 50'
                WHEN umur >= 50 THEN '50 < ...'
                WHEN umur IS NULL THEN '(NULL)'
            END as range_umur,
            COUNT(*) AS jumlah
            
            FROM (select nama, nik, tanggal_lahir, TIMESTAMPDIFF(YEAR, tanggal_lahir, CURDATE()) AS umur from tbl_employee)  as employee
            GROUP BY range_umur
            ORDER BY range_umur
        "));

        return $bar;

    }
}
