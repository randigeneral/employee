<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Users extends Model
{

    // protected $table = 'm_user';
    // protected $primaryKey = 'user_id';
    // public $incrementing = false;
    // public $timestamps = false;

    // protected $fillable = [
    //     'user_id'
    //     ,'user_email'
    //     ,'user_firstname'
    //     ,'user_lastname'
    //     ,'user_gender'
    //     ,'user_phone'
    //     ,'user_image'
    //     ,'user_password'
    //     ,'user_pin'
    //     ,'user_pin_status'
    //     ,'status'
    //     ,'created_by'
    //     ,'updated_by'
    //     ,'created_at'
    //     ,'updated_at'
    // ];

    public function findByEmail($email){
        $user = DB::table('m_user')->where('user_email',$email)->first();
        return $user;
    }

}
